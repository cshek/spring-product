package com.citi.training.product;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.citi.training.product.repo.InMemoryProductRepository;

public class InMemoryProductRepositoryTests {

	@Test
	public void test_AddingProducts() {
		InMemoryProductRepository test = new InMemoryProductRepository();
		Product testProd = new Product(1, "Phone", 1000);
		test.saveProduct(testProd);
		assert(test.getProduct(1).equals(testProd));
	}
	
	@Test
	public void test_GetAllProducts() {
		InMemoryProductRepository test = new InMemoryProductRepository();
		List<Product> productList = new ArrayList<>();
		Product testProd = new Product(1, "Phone", 1000);
		productList.add(testProd);
		test.saveProduct(testProd);
		assert(test.getAllProducts().get(0).equals(productList.get(0)));
	}

}
