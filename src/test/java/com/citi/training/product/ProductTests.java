package com.citi.training.product;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProductTests {

	@Test
	public void test_getProduct() {
		Product test = new Product(1, "Phone", 1000);
		
		assert(test.getId() == 1);
		assert(test.getName().equals("Phone"));
		assert(test.getPrice() == 1000);
	}

}
