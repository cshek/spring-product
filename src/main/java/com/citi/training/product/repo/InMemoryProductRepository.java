package com.citi.training.product.repo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.citi.training.product.Product;

@Component
public class InMemoryProductRepository implements ProductRepository {

	private HashMap<Integer, Product> products;
	
	public InMemoryProductRepository()
	{
		products = new HashMap<Integer, Product>();
	}
	
	@Override
	public void saveProduct(Product product) {
		products.put(product.getId(), product);
	}

	@Override
	public Product getProduct(int id) {
		return products.get(id);
	}

	@Override
	public List<Product> getAllProducts() {
		return new ArrayList<Product>(products.values());
	}

}
