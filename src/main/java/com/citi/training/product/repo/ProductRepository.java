package com.citi.training.product.repo;

import java.util.List;

import com.citi.training.product.Product;

public interface ProductRepository {

	void saveProduct(Product product);
	
	Product getProduct(int id);
	
	List<Product> getAllProducts();
	
}
