package com.citi.training.product.menu;

import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.citi.training.product.Product;
import com.citi.training.product.repo.ProductRepository;

@Component
@Profile("default")
public class Menu implements ApplicationRunner{
	
	@Autowired
	ProductRepository productRepo;

	public void run(ApplicationArguments args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("How many items do you want to create: ");
		int numItems = sc.nextInt();
		for(int i = 0; i < numItems; i++)
		{
			System.out.print("Provide id: ");
			int id = sc.nextInt();
			sc.nextLine();
			System.out.print("Provide name: ");
			String name = sc.nextLine();
			System.out.print("Provide price: ");
			double price = sc.nextDouble();
			
			Product prod = new Product(id, name, price);
			productRepo.saveProduct(prod);
			System.out.println("Product Created\n");
		}
		
		List<Product> products = productRepo.getAllProducts();
		
		System.out.println("Listing products: ");
		for(Product product : products)
		{
			System.out.println(product);
		}
		
		sc.close();
	}

}
