package com.citi.training.product.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.citi.training.product.Product;
import com.citi.training.product.repo.ProductRepository;

@Component
@Profile("demo")
public class Demo implements ApplicationRunner {
	
	@Autowired
	ProductRepository productRepo;

	public void run(ApplicationArguments args)
	{
		Product prod1 = new Product(1, "Phone", 1000);
		Product prod2 = new Product(2, "Ice Cream", 1.25);
		Product prod3 = new Product(3, "McDonalds", 6.30);
		
		productRepo.saveProduct(prod1);
		productRepo.saveProduct(prod2);
		productRepo.saveProduct(prod3);
		
		List<Product> products = productRepo.getAllProducts();
		
		for(Product product : products)
		{
			System.out.println(product);
		}
	}
}
